import React, { useEffect } from "react";
import "../App.scss";
import ErrorBoundary from "../components/ErrorBoundary";
import Panel from "../components/Panel";
import MenuButton from "../components/MenuButton";
import Box from "../components/Box";
import Spinner from "../components/Spinner";
import TimeLine from "../components/TimeLine";
import Expertise from "../components/Expertise";
import Skills from "../components/Skills";
import Portfolio from "../components/Portfolio";
import Contacts from "../components/Contacts";
import Feedbacks from "../components/Feedbacks";
import UpButton from "../components/UpButton";
import dataExperience from "../data/dataForExperience";
import dataContacts from "../data/dataForContacts";
import dataFeedbacks from "../data/dataForFeedbacks";
import { useSelector, useDispatch } from "react-redux";
import { getEducations } from "../features/education/educationSlice";
import { getSkills } from "../features/skills/skillsSlice";

function InnerPage() {
  const dataEducation = useSelector((state) => state.educations.entities.educations);
  const stateLoading = useSelector((state) => state.educations.loading);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getEducations());
    dispatch(getSkills());
  }, [dispatch]);

  return (
    <>
      <ErrorBoundary>
        <div className="main-container">
          <Panel />
          <section className="content" id="section-content">
            <MenuButton />
            <div className="content-details">
              <Box
                title="About me"
                content="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque"
              />
              <div>
                {stateLoading ? (
                  <Spinner titleEducation="Education" />
                ) : (
                  <TimeLine titleEducation="Education" data={dataEducation} />
                )}
              </div>
              <Expertise title="Experience" data={dataExperience} />
              <Skills title="Skills" />
              <Portfolio title="Portfolio" />
              <Contacts title="Contacts" data={dataContacts} />
              <Feedbacks title="Feedbacks" data={dataFeedbacks} />
            </div>
            <UpButton />
          </section>
        </div>
      </ErrorBoundary>
    </>
  );
}

export default InnerPage;
