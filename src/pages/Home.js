import React from "react";
import "../App.scss";
import PhotoBox from "../components/PhotoBox";
import Button from "../components/Button";
import { Link, Outlet } from "react-router-dom";

export default function HomePage() {
  return (
    <>
      <div className="home-container">
        <div className="layer">
          <div className="user-container">
            <PhotoBox
              name="Volha Ivanova"
              title="Front-End Developer"
              description="Enthusiastic and hard-working, I am interested in the Information Technology field. 
                            Invited to join UpSkill Front-End Course, I gained skills and experience in this area. To develop and grow as a JavaScript developer 
                            I am looking for a job or internship in a company where I can apply and multiply my knowledge and skills 
                            and help to develop apps and build reusable code."
            />
            <Link to="/inner">
              <Button className="button-knowMore" text="Know more" data-testid="home-page"/>
            </Link>
            <Outlet />
          </div>
        </div>
      </div>
    </>
  );
}
