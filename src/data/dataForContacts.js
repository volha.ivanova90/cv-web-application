import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { BsTwitter } from 'react-icons/bs';
import { GrFacebookOption } from 'react-icons/gr';
import { AiFillSkype } from 'react-icons/ai';



const dataContacts = [ 
    { id:1, icon: <FontAwesomeIcon icon="phone" flip="horizontal" />, info: { description: '500 342 242', title: '' }, link: "tel:+500 342 242" },
    { id:2, icon: <FontAwesomeIcon icon="envelope" />, info: { description: 'office@kamsolutions.pl', title: ''}, link: "mailto:office@kamsolutions.pl" },
    { id:3, icon: <BsTwitter />, info: { title: 'Twitter', description: 'https://twitter.com/wordpress' }, link: "https://twitter.com/wordpress" },
    { id:4, icon: <GrFacebookOption />, info: { title: 'Facebook', description: 'https://www.facebook.com/facebook' }, link: "https://www.facebook.com/facebook" },
    { id:5, icon: <AiFillSkype />, info: { title: 'Skype', description: 'kamsolutions.pl' }, link: "skype:kamsolutions.pl" }
];

export default dataContacts;

