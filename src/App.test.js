import React from 'react';
import { render, screen } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import Home from './pages/Home';
import Form from './components/Form';
import Navigation from './components/Navigation';
import Contacts from './components/Contacts';
import Box from './components/Box';
import store from './features/store';
import reducerThunkEducation from './features/education/educationSlice';
import reducerThunkSkills from './features/skills/skillsSlice';
import { getEducations } from './features/education/educationSlice';
import { getSkills} from './features/skills/skillsSlice';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


test('renders learn react link', () => {
  render(<Router><Provider store={store}><Navigation /></Provider></Router>);
  render(<Router><Provider store={store}><Box /></Provider></Router>);
  const linkElement = screen.getByText(/About me/i);
  expect(linkElement).toBeInTheDocument();
});

// test Home page
test('Know more button has text Know more', () => {
  render(<Router><Provider store={store}><Home /></Provider></Router>);
  const knowMoreButton = screen.getByText(/Know more/i);
  expect(knowMoreButton).toHaveTextContent('Know more');
});

test('Logo must have src={userAvatar} and alt = "avatar"', () => {
  render(<Router><Provider store={store}><Home /></Provider></Router>);
  const logo = screen.getByRole('img');
  expect(logo).toHaveAttribute('src', 'user_avatar.png');
  expect(logo).toHaveAttribute('alt', 'avatar');
});

test('h1 is Front-End developer', () => {
  render(<Router><Provider store={store}><Home /></Provider></Router>);
  const h1 = screen.getByText(/Front-End Developer/i);
  expect(h1).toHaveTextContent('Front-End Developer');
});

// test Form

test('should show input placeholder in Skill Name Input', () => {
  render(<Router><Provider store={store}><Form /></Provider></Router>);
  const input = screen.getByPlaceholderText('Enter skill name');
  expect(input).toBeInTheDocument();
})

test('should show input placeholder in Skill Range Input', () => {
  render(<Router><Provider store={store}><Form /></Provider></Router>);
  const input = screen.getByPlaceholderText('Enter skill range');
  expect(input).toBeInTheDocument();
})


// testing reducers
test('getEducations - initial state: entities is empty array and loading false', () => {
  expect(reducerThunkEducation(undefined, getEducations())).toEqual({entities: [], loading: false});
});

test('getSkills - initial state : entities is empty array and loading false', () => {
  expect(reducerThunkSkills(undefined, getSkills())).toEqual({entities: [], loading: false, uploading: false});
});

// Contacts

test('renders contacts', () => {
  const fakeContacts = [{id:1, icon: <FontAwesomeIcon icon="phone" flip="horizontal" />, info: { description: '500 342 242', title: '' }, link: "tel:+500 342 242" }]
  render(<Router><Provider store={store}><Contacts data={fakeContacts} /></Provider></Router>);
  const dataContacts = screen.getAllByTestId('single-contact').map(li => li.textContent);
  const fakeContactsData = fakeContacts.map(contact => contact.info.description);
  expect(dataContacts).toEqual(fakeContactsData);
});






