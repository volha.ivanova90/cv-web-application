import "../App.scss";
import "../assets/styles/SkillsDisplayed.scss";
import { useSelector } from "react-redux";

function SkillLine(props) {
  return (
    <>
      <li
        key={props.skillName}
        className="skills-line-container__element"
        style={{ width: props.skillRange + `${"%"}` }}
      >
        <span
          key={props.skillName}
          className="skills-line-container__element__text"
        >
          {props.skillName}
        </span>
      </li>
    </>
  );
}

export default function SkillsDisplayed() {
  const data = useSelector((state) => state.skills.entities);

  console.log(data);

  if (data !== undefined) {
    return (
      <>
        <ul className="skills-line-container">
          {data.map((skill) => (
            <SkillLine
              key={skill.skillName}
              skillName={skill.skillName}
              skillRange={skill.skillRange}
            />
          ))}
        </ul>
      </>
    );
  } else {
    return null;
  }
}
