import React from "react";

export default function Button(props) {
  return (
    <>
      <button
        className={`button-knowMore ${props.icon ? "button-goBack" : ""}`}
      >
        {props.icon}
        <span className="button-goBack__text">{props.text}</span>
      </button>
    </>
  );
}
