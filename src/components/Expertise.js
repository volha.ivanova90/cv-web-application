import React from "react";
import "../App.scss";
import "../assets/styles/Expertise.scss";

function SingleExpertise(props) {
  return (
    <li>
      <div className="expertise-list-date">
        <h3>{props.company}</h3>
        <span className="date">{props.date}</span>
      </div>
      <div className="expertise-list-info">
        <h3>{props.job}</h3>
        <p>{props.description}</p>
      </div>
    </li>
  );
}

export default function Expertise(props) {
  return (
    <>
      <div className="expertise-container" id="experience" >
        <h2 className="experience inner-title">{props.title}</h2>
        <div className="expertise" data-testid="expertise">
          <ul className="expertise-list">
            {props.data.map((data) => (
              <SingleExpertise
                key={data.date}
                company={data.info.company}
                date={data.date}
                job={data.info.job}
                description={data.info.description}
              />
            ))}
          </ul>
        </div>
      </div>
    </>
  );
}
