import React from "react";
import { useSelector } from "react-redux";
import "../App.scss";
import "../assets/styles/TimeLine.scss";

function SingleEducation(props) {
  return (
    <li>
      <div className="timeline-date" key={props.date}>
        {props.date}
      </div>
      <div className="timeline-event">
        <div className="info">
          <h3 key={props.title}>{props.title}</h3>
          <p key={props.text}>{props.text}</p>
        </div>
      </div>
    </li>
  );
}

export default function TimeLine(props) {
  const dataEducation = useSelector(
    (state) => state.educations.entities.educations
  );

  if (dataEducation) {
    return (
      <>
        <div className="timeline-container" id="education">
          <h2 className="inner-title">{props.titleEducation}</h2>
          <div className="timeline">
            <ul className="timeline-list">
              {props.data?.map((data) => (
                <SingleEducation
                  key={data.date}
                  date={data.date}
                  title={data.title}
                  text={data.text}
                />
              ))}
            </ul>
          </div>
        </div>
      </>
    );
  } else {
    return (
      <>
        <div className="timeline-container" id="education">
          <h2 className="inner-title">{props.titleEducation}</h2>
          <div className="timeline-error">
            <span>
              Something went wrong; please review your server connection!
            </span>
          </div>
        </div>
      </>
    );
  }
}
