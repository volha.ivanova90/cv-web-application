import React from "react";
import { Form, Field } from "react-final-form";
import { useDispatch } from "react-redux";
import "../App.scss";
import "../assets/styles/Form.scss";
import { postSkills } from "../features/skills/skillsSlice";

export default function SkillsForm() {
  const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

  const dispatch = useDispatch();

  const onSubmit = async (values) => {
    await sleep(300);
    let skills = {
      skillName: values.SkillName,
      skillRange: values.SkillRange,
    };
    const skillsArray = [
      ...JSON.parse(localStorage.getItem("skills") || "[]"),
      skills,
    ];
    localStorage.setItem("skills", JSON.stringify(skillsArray));
    dispatch(postSkills(skillsArray));
  };

  const SkillNameRequired = (value) =>
    value ? undefined : "Skill name is a required field";
  const SkillRangeRequired = (value) =>
    value ? undefined : "Skill range is a required field";

  const composeValidators =
    (...validators) =>
    (value) =>
      validators.reduce(
        (error, validator) => error || validator(value),
        undefined
      );

  const isLetter = (value) => {
    if (typeof value !== undefined) {
      if (!value.match(/^[a-zA-Z]+$/)) {
        return "Please enter letters only";
      }
    }
  };

  const isNumber = (value) => {
    if (typeof value !== undefined) {
      if (!value.match(/^[0-9]*$/)) {
        return "Skill range must be a 'number' type";
      }
    }
  };

  const isMoreThenLowestValue = (value) => {
    if (value < 10) {
      return "Skill range must be greater than or equal to 10";
    }
  };

  const isLessThenHighestValue = (value) => {
    if (value > 100) {
      return "Skill range must be less than or equal to 100";
    }
  };    
    
  return (
    <div className="form-container" id="form">
      <Form
        onSubmit={onSubmit}
        render={({ handleSubmit, form, submitting, pristine, values }) => (
          <form onSubmit={handleSubmit} className="form">
            <div className="form-container_element">
              <Field
                name="SkillName"
                validate={composeValidators(SkillNameRequired, isLetter)}
              >
                {({ input, meta }) => (
                  <div>
                    <label className="input-name">Skill name:</label>
                    <input
                      {...input}
                      className="input"
                      type="text"
                      placeholder="Enter skill name"
                    />
                    {meta.error && meta.touched && (
                      <span aria-labelledby="username" className="error">
                        {meta.error}
                      </span>
                    )}
                  </div>
                )}
              </Field>
            </div>
            <div className="form-container_element">
              <Field
                name="SkillRange"
                validate={composeValidators(
                  SkillRangeRequired,
                  isNumber,
                  isMoreThenLowestValue,
                  isLessThenHighestValue
                )}
              >
                {({ input, meta }) => (
                  <div>
                    <label className="input-name">Skill range:</label>
                    <input
                      {...input}
                      className="input"
                      type="text"
                      placeholder="Enter skill range"
                    />
                    {meta.error && meta.touched && (
                      <span className="error">{meta.error}</span>
                    )}
                  </div>
                )}
              </Field>
            </div>
            <div className="buttons">
              <button
                type="submit"
                className="submit-btn"
                disabled={
                  submitting ||
                  pristine ||
                  values.SkillName === undefined ||
                  values.SkillRange === undefined
                }
              >
                <span className="submit-btn__text">Add skill</span>
              </button>
            </div>
          </form>
        )}
      />
    </div>
  );
}
