import React, { useEffect, useState } from "react";
import { HiMenu } from "react-icons/hi";

export default function MenuButton() {
  const [showNav, setShowNav] = useState(false);
  useEffect(() => {
    const nav = document.getElementById("navbar");
    nav.style.display = showNav ? "none" : "block";
  }, [showNav]);

  return (
    <>
      <button
        type="button"
        className="icon-menu-btn"
        onClick={() => setShowNav(!showNav)}
        id="menu"
      >
        {<HiMenu className="icon-menu-btn__icon" />}
      </button>
    </>
  );
}
