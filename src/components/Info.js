import React from "react";
import "../App.scss";

export default function Info(props) {
  return (
    <>
      <p className="info-text">{props.text}</p>
    </>
  );
}
