import React from "react";
import "../App.scss";
import "../assets/styles/Contacts.scss";

function SingleContact(props) {
  return (
    <li className="single-contact">
      <span className="single-contact__icon" key={props.icon}>
        {props.icon}
      </span>
      <div className="single-contact__address">
        <h3 key={props.title}>{props.title}</h3>
        <p key={props.description}>
          <a
            href={props.link}
            target="_blank"
            rel="noreferrer"
            key={props.link}
          >
            {props.description}
          </a>
        </p>
      </div>
    </li>
  );
}

export default function Contacts(props) {
  return (
    <>
      <div className="contacts-container" id="contacts" data-testid="single-contact">
        <h2 className="inner-title">{props.title}</h2>
        <ul className="contacts-container__content">
          {props.data.map((data) => (
            <SingleContact 
              key={data.id}
              icon={data.icon}
              title={data.info.title}
              description={data.info.description}
              link={data.link}
            />
          ))}
        </ul>
      </div>
    </>
  );
}
