import React from "react";
import "../App.scss";
import { BsFillPersonFill } from "react-icons/bs";
import { IoIosSchool, IoIosPaperPlane } from "react-icons/io";
import { RiPencilFill } from "react-icons/ri";
import { FaRegGem } from "react-icons/fa";
import { GiSuitcase } from "react-icons/gi";
import { BiMessageRounded } from "react-icons/bi";

export default function Navigation() {
  const goToAbout = (e) => {
    e.preventDefault();
    const titleElement = document.getElementById("aboutme");
    titleElement.scrollIntoView({ behavior: "smooth" });
  };

  const goToEducation = (e) => {
    e.preventDefault();
    const titleElement = document.getElementById("education");
    titleElement.scrollIntoView({ behavior: "smooth" });
  };

  const goToExperience = (e) => {
    e.preventDefault();
    const titleElement = document.getElementById("experience");
    titleElement.scrollIntoView({ behavior: "smooth" });
  };

  const goToSkills = (e) => {
    e.preventDefault();
    const titleElement = document.getElementById("skills");
    titleElement.scrollIntoView({ behavior: "smooth" });
  };

  const goToPortfolio = (e) => {
    e.preventDefault();
    const titleElement = document.getElementById("portfolio");
    titleElement.scrollIntoView({ behavior: "smooth" });
  };

  const goToContacts = (e) => {
    e.preventDefault();
    const titleElement = document.getElementById("contacts");
    titleElement.scrollIntoView({ behavior: "smooth" });
  };

  const goToFeedbacks = (e) => {
    e.preventDefault();
    const titleElement = document.getElementById("feedbacks");
    titleElement.scrollIntoView({ behavior: "smooth" });
  };

  return (
    <>
      <div className="nav-container">
        <ul>
          <li className="nav-item">
            <button className="nav-item__btn" onClick={goToAbout}>
              <BsFillPersonFill className="nav-icon" />
              <span className="nav-item__text"> About me </span>
            </button>
          </li>
          <li className="nav-item">
            <button className="nav-item__btn" onClick={goToEducation}>
              <IoIosSchool className="nav-icon" />
              <span className="nav-item__text"> Education </span>
            </button>
          </li>
          <li className="nav-item">
            <button className="nav-item__btn" onClick={goToExperience}>
              <RiPencilFill className="nav-icon" />
              <span className="nav-item__text"> Experience </span>
            </button>
          </li>
          <li className="nav-item">
            <button className="nav-item__btn" onClick={goToSkills}>
              <FaRegGem className="nav-icon" />
              <span className="nav-item__text"> Skills </span>
            </button>
          </li>
          <li className="nav-item">
            <button className="nav-item__btn" onClick={goToPortfolio}>
              <GiSuitcase className="nav-icon" />
              <span className="nav-item__text"> Portfolio </span>
            </button>
          </li>
          <li className="nav-item">
            <button className="nav-item__btn" onClick={goToContacts}>
              <IoIosPaperPlane className="nav-icon" />
              <span className="nav-item__text"> Contacts </span>
            </button>
          </li>
          <li className="nav-item">
            <button className="nav-item__btn" onClick={goToFeedbacks}>
              <BiMessageRounded className="nav-icon" />
              <span className="nav-item__text"> Feedbacks </span>
            </button>
          </li>
        </ul>
      </div>
    </>
  );
}
