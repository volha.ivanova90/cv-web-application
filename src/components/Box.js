import React from "react";
import "../App.scss";

export default function Box(props) {
  return (
    <>
      <div className="box-about" id="aboutme" >
        <h2 className="inner-title box-title" data-testid="about-me">{props.title}</h2>
        <p className="box-content">{props.content}</p>
      </div>
    </>
  );
}
