import React from "react";
import "../App.scss";
import userAvatar from "../assets/images/user_avatar.png";

export default function PhotoBox(props) {
  return (
    <>
      <img
        className={`user-avatar__home ${
          props.description ? "user-avatar__home" : "user-avatar__inner"
        }`}
        src={userAvatar}
        alt="avatar"
      />
      <h1 
        className={`user-name__home ${
          props.description ? "user-name__home" : "user-name__inner"
        }`}
      >
        {props.userName}
      </h1>
      <h2 className="user-position">{props.title}</h2>
      <p className="user-text">{props.description}</p>
    </>
  );
}
