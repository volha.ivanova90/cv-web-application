import React from "react";
import "../App.scss";
import { IoIosArrowUp } from "react-icons/io";

export default function UpButton() {
  
  const scrollToTop = (e) => {
    e.preventDefault();
    const titleElement = document.getElementById("aboutme");
    titleElement.scrollIntoView({ behavior: "smooth" });
  };

  return (
    <>
      <button
        type="button"
        onClick={scrollToTop}
        className="up-btn"
      >
        {<IoIosArrowUp className="up-btn__arrow-up" />}
      </button>
    </>
  );
}
