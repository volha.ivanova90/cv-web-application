import React, { useState } from "react";
import SkillsForm from "./Form";
import SkillsDisplayed from "./SkillsDisplayed";
import { FaEdit } from "react-icons/fa";
import "../App.scss";
import "../assets/styles/Skills.scss";

export default function Skills(props) {
  const [showForm, setShowForm] = useState(false);

  const openForm = (e) => {
    e.preventDefault();
    setShowForm(true);
  };

  return (
    <>
      <div className="skills-container" id="skills">
        <div className="skills-container-flex">
          <h2 className="inner-title">{props.title}</h2>
          <button type="button" className="edit-btn" onClick={openForm}>
            <FaEdit className="edit-btn__icon" />
            <span className="edit-btn__text">Open Edit</span>
          </button>
        </div>
        {showForm && <SkillsForm />}
        <SkillsDisplayed />
        <div className="skills-scale">
          <div className="skills-scale-vertical">
            <div className="skills-scale__vertical-line line-1"></div>
            <div className="skills-scale__vertical-line line-2"></div>
            <div className="skills-scale__vertical-line line-3"></div>
            <div className="skills-scale__vertical-line line-4"></div>
          </div>
          <div className="skills-scale__horizontal-line"></div>
          <div className="level">
            <div className="level-name name-1">Beginner</div>
            <div className="level-name name-2">Proficient</div>
            <div className="level-name name-3">Expert</div>
            <div className="level-name name-4">Master</div>
          </div>
        </div>
      </div>
    </>
  );
}
