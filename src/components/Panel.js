import "../App.scss";
import PhotoBox from "./PhotoBox";
import Navigation from "./Navigation";
import Button from "./Button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";

export default function Panel() {
  return (
    <>
      <section id="navbar">
        <PhotoBox userName="Volha Ivanova" />
        <Navigation />
        <Link to="/">
          <Button
            icon={
              <FontAwesomeIcon className="chevron-icon" icon="chevron-left" />
            }
            text="Go back"
          />
        </Link>
      </section>
    </>
  );
}
