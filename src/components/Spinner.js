import React from "react";
import "../App.scss";
import "../assets/styles/Spinner.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function Spinner(props) {
  return (
    <>
      <div className="timeline-container" id="education">
        <h2 className="inner-title">{props.titleEducation}</h2>
        <div className="spinner">
          <i className="fa fa-refresh fa-3x">
            <FontAwesomeIcon className="icon" icon="sync-alt" />
          </i>
        </div>
      </div>
    </>
  );
}
