import React, { useState, useEffect } from "react";
import "../App.scss";
import "../assets/styles/Portfolio.scss";
import card_1 from "../assets/cards/card_1.png";
import card_3 from "../assets/cards/card_3.png";
import Isotope from "isotope-layout";

export default function Portfolio(props) {
  const [isotope, setIsotope] = useState(null);
  const [filterKey, setFilterKey] = useState("*");
  useEffect(() => {
    setIsotope(
      new Isotope(".filter-container", {
        itemSelector: ".filter-item",
      })
    );
  }, []);
  useEffect(() => {
    if (isotope) {
      filterKey === "*"
        ? isotope.arrange({ filter: `*` })
        : isotope.arrange({ filter: `.${filterKey}` });
    }
  }, [isotope, filterKey]);

  return (
    <>
      <div className="portfolio-container" id="portfolio">
        <h2 className="inner-title">{props.title}</h2>
        <div className="portfolio-container__content">
          <ul className="tabs">
            <button onClick={() => setFilterKey("*")}>
              <li className="tabs__all">
                <span> all </span>
              </li>
            </button>
            <button onClick={() => setFilterKey("ui")}>
              <li className="tabs__ui">
                <span> ui </span>
              </li>
            </button>
            <button onClick={() => setFilterKey("code")}>
              <li className="tabs__code">
                <span> code </span>
              </li>
            </button>
          </ul>
          <ul className="filter-container">
            <li className="filter-item ui">
              <div className="filter-item__first">
              
                <div className="portfolio-info portfolio-info__first">
                  <h2>Some text</h2>
                  <p>
                    Donec pede justo, fringilla vel, aliquet nec, vulputate
                    eget, arcu. In enim justo, rhoncus ut, imperdiet a,
                    venenatis vitae, justo. Nullam dictum felis eu pede mollis{" "}
                  </p>
                  <a href="https://somesite.com">View source</a>
                </div>
                <img src={card_1} alt="card_1" />
              </div>
            </li>
            <li className="filter-item code">
              <div className="filter-item__second">
                <div className="portfolio-info portfolio-info__second">
                  <h2>Some text</h2>
                  <p>
                    Donec pede justo, fringilla vel, aliquet nec, vulputate
                    eget, arcu. In enim justo, rhoncus ut, imperdiet a,
                    venenatis vitae, justo. Nullam dictum felis eu pede mollis{" "}
                  </p>
                  <a href="https://somesite.com">View source</a>
                </div>
                <img src={card_3} alt="card_3" />
              </div>
            </li>
            <li className="filter-item ui">
              <div className="filter-item__third">
                <div className="portfolio-info portfolio-info__third">
                  <h2>Some text</h2>
                  <p>
                    Donec pede justo, fringilla vel, aliquet nec, vulputate
                    eget, arcu. In enim justo, rhoncus ut, imperdiet a,
                    venenatis vitae, justo. Nullam dictum felis eu pede mollis{" "}
                  </p>
                  <a href="https://somesite.com">View source</a>
                </div>
                <img src={card_1} alt="card" />
              </div>
            </li>
            <li className="filter-item code">
              <div className="filter-item__forth">
                <div className="portfolio-info portfolio-info__forth">
                  <h2>Some text</h2>
                  <p>
                    Donec pede justo, fringilla vel, aliquet nec, vulputate
                    eget, arcu. In enim justo, rhoncus ut, imperdiet a,
                    venenatis vitae, justo. Nullam dictum felis eu pede mollis{" "}
                  </p>
                  <a href="https://somesite.com">View source</a>
                </div>
                <img src={card_3} alt="card" />
              </div>
            </li>
          </ul>
        </div>
      </div>
    </>
  );
}
