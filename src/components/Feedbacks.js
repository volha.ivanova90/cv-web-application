import React from "react";
import "../App.scss";
import "../assets/styles/Feedbacks.scss";
import Info from "./Info";

export default function Feedbacks(props) {
  function SingleFeedback(props) {
    return (
      <li className="single-feedback">
        <aside className="single-feedback__source">
          <img
            className="single-feedback__avatar"
            key={props.avatar}
            src={props.avatar}
            alt="avatar"
          />
          <p>
            <span className="single-feedback__name" key={props.name}>
              {props.name}
            </span>
            <span className="single-feedback__url" key={props.website}>
              <a href={props.url} key={props.url}>
                {props.website}
              </a>
            </span>
          </p>
        </aside>
      </li>
    );
  }
  return (
    <>
      <div className="feedbacks-container" id="feedbacks" data-testid="feedback-info">
        <h2 className="inner-title">{props.title}</h2>
        <ul className="feedbacks-container__content">
          {props.data.map((data) => (
            <div key={data.id}>
              <Info text={data.text} />
              <SingleFeedback
                key={data.name}
                avatar={data.avatar}
                name={data.name}
                website={data.website}
                url={data.url}
              />
            </div>
          ))}
        </ul>
      </div>
    </>
  );
}
