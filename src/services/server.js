import { createServer, Model } from "miragejs";

export default function Server() {  

  createServer({

  
      models: {
        skills: Model,
     },

    routes() {
      this.namespace = "api";

      this.get(
        "/educations",
        () => {
          return {
            educations: [
              {
                date: "2020",
                title: "Title 0",
                text: "Elit voluptate ad nostrud laboris. Elit incididunt mollit enim enim id id laboris dolore et et mollit. Mollit adipisicing ullamco exercitation ullamco proident aute enim nisi. Dolore eu fugiat consectetur nulla sunt Lorem ex ad. Anim eiusmod do tempor fugiat minim do aliqua amet ex dolore velit.",
              },
              {
                date: "2018",
                title: "Title 1",
                text: "Et irure culpa ad proident labore excepteur elit dolore. Quis commodo elit culpa eiusmod dolor proident non commodo excepteur aute duis duis eu fugiat. Eu duis occaecat nulla eiusmod non esse cillum est aute elit amet cillum commodo.",
              },
              {
                date: "2017",
                title: "Title 2",
                text: "Labore esse tempor nisi non mollit enim elit ullamco veniam elit duis nostrud. Enim pariatur ullamco dolor eu sunt ad velit aute eiusmod aliquip voluptate. Velit magna labore eiusmod eiusmod labore amet eiusmod. In duis eiusmod commodo duis. Exercitation Lorem sint do aliquip veniam duis elit quis culpa irure quis nulla. Reprehenderit fugiat amet sint commodo ex.",
              },
              {
                date: "2016",
                title: "Title 3",
                text: "Labore esse tempor nisi non mollit enim elit ullamco veniam elit duis nostrud. Enim pariatur ullamco dolor eu sunt ad velit aute eiusmod aliquip voluptate. Velit magna labore eiusmod eiusmod labore amet eiusmod. In eiusmod commodo duis. Exercitation Lorem sint do aliquip veniam duis elit quis culpa irure quis nulla. Reprehenderit fugiat amet sint commodo ex.",
              },
              {
                date: "2010",
                title: "Title 4",
                text: "Elit voluptate ad nostrud laboris. Elit incididunt mollit enim enim id id laboris dolore et et mollit. Mollit adipisicing ullamco exercitation ullamco proident aute enim nisi. Dolore eu fugiat consectetur nulla sunt Lorem ex ad. Anim eiusmod do tempor fugiat do aliqua amet ex dolore velit.",
              },
              {
                date: "2008",
                title: "Title 5",
                text: "Elit ad nostrud laboris. Elit incididunt mollit enim enim id id laboris dolore et et mollit. Mollit adipisicing ullamco exercitation ullamco proident aute enim nisi. Dolore eu fugiat consectetur nulla sunt Lorem ex ad. Anim eiusmod do tempor fugiat do aliqua amet ex dolore velit.",
              },
              {
                date: "2005",
                title: "Title 6",
                text: "Elit voluptate ad nostrud laboris. Elit incididunt mollit enim enim id id laboris dolore et et mollit. Mollit adipisicing ullamco exercitation ullamco proident aute enim nisi. Dolore eu fugiat consectetur nulla sunt Lorem ex ad. Anim eiusmod do tempor fugiat do aliqua amet ex dolore velit.",
              },
            ],
          };
        },
        { timing: 3000 }
      );

      this.get("/skills", (schema) => {
        console.log(schema.skills.all());
        return schema.skills.all;
      });

      this.post("/skills", (schema, request) => {
        let skillsValue = JSON.parse(request.requestBody);
        console.log(skillsValue);
        return schema.skills.create(skillsValue);
      });      
    },
  });
}
