import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Routes } from "react-router-dom";
import HomePage from "./pages/Home";
import InnerPage from "./pages/Inner";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faChevronLeft,
  faBars,
  faPhone,
  faEnvelope,
  faAngleUp,
  faSyncAlt,
} from "@fortawesome/free-solid-svg-icons";
library.add(faChevronLeft, faBars, faPhone, faEnvelope, faAngleUp, faSyncAlt);

function App() {
  return (
    <>
      <Router>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/inner" element={<InnerPage />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
