import { configureStore } from "@reduxjs/toolkit";
import reducers from "../features/reducers/index";

const store = configureStore({
  reducer: reducers,
});

export default store;
