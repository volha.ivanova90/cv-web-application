import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

export const getEducations = createAsyncThunk(
  "get/getEducations",
  async (_, { rejectWithValue }) => {
    try {
      const response = await fetch("/api/educations/", {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      });
      const data = await response.json();
      return data;
    } catch (err) {
      return rejectWithValue(`error - ${err}`);
    }
  }
);

const reducerThunkEducation = createSlice({
  name: "educations",
  initialState: { entities: [], loading: false },
  reducers: {},
  extraReducers: {
    [getEducations.pending]: (state) => {
      return {
        ...state,
        loading: true,
      };
    },
    [getEducations.fulfilled]: (state, { payload }) => {
      return {
        ...state,
        loading: false,
        entities: payload,
      };
    },
    [getEducations.rejected]: (state) => {
      return {
        ...state,
        loading: false,
      };
    },
  },
});

export default reducerThunkEducation.reducer;
