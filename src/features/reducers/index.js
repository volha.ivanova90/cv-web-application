import { combineReducers } from "redux";
import reducerThunkEducation from "../education/educationSlice";
import reducerThunkSkills from "../skills/skillsSlice";

const reducers = combineReducers({
  educations: reducerThunkEducation,
  skills: reducerThunkSkills,
});

export default reducers;
