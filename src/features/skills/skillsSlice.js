import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

export const getSkills = createAsyncThunk(
  "skills/getSkills",
  async (_, { rejectWithValue }) => {
    try {
      if (localStorage.getItem("skills")) {
        return JSON.parse(localStorage.getItem("skills"));
      } else {
        const response = await fetch("/api/skills/", {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        });
        const data = await response.json();
        return data;
      }
    } catch (err) {
      return rejectWithValue(`error - ${err}`);
    }
  }
);

export const postSkills = createAsyncThunk(
  "skills/postSkills",
  async (skills, { rejectWithValue }) => {
    try {
      const response = await fetch("/api/skills/", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(skills),
      });
      return skills;
    } catch (err) {
      return rejectWithValue(`error - ${err}`);
    }
  }
);

const reducerThunkSkills = createSlice({
  name: "skills",
  initialState: { entities: [], loading: false, uploading: false },
  reducers: {},
  extraReducers: {
    [getSkills.pending]: (state) => {
      return {
        ...state,
        loading: true,
      };
    },
    [getSkills.fulfilled]: (state, { payload }) => {
      return {
        ...state,
        loading: false,
        entities: payload,
      };
    },
    [getSkills.rejected]: (state) => {
      return {
        ...state,
        loading: false,
      };
    },
    [postSkills.pending]: (state) => {
      return {
        ...state,
        loading: true,
        uploading: false,
      };
    },
    [postSkills.fulfilled]: (state, { payload } ) => {
      return {
        ...state,
        loading: false,
        entities: payload,
        uploading: true,
      };
    },
    [postSkills.rejected]: (state) => {
      return {
        ...state,
        loading: false,
        uploading: false,
      };
    },
  },
});

export default reducerThunkSkills.reducer;
